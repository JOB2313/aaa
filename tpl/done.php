<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* extract settings for the text */
extract($settings, EXTR_PREFIX_ALL, "set");

/* get a list of generated questions for the text */
$q = implode(", ", $set_exam);

/* get singular/plural */
$qword = $set_count > 1 ? $content["questions_pl"] : $content["questions_sg"];

/* done page HTML body */
$content["body"] = <<<CNT_DONE
<p><strong>{$content["done_picked"]}</strong></p>
<p>{$content["done_text"]}</p>
<blockquote><span>$q</span></blockquote>
<p>{$content["done_details"]} $set_count $qword {$content["from"]} $set_min
{$content["to"]} $set_max.</p>
CNT_DONE;

/* basic HTML template */
include "template.php";